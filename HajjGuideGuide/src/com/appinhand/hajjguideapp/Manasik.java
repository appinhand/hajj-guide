package com.appinhand.hajjguideapp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.appinhand.hajjguideapp.R;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class Manasik extends Activity {
	
	
	Button share,home,info,Exit,back;
	public static Facebook mFacebook;
    public static AsyncFacebookRunner mAsyncRunner;
    
	
    Button Manasik,History,Stream,Quotes;
	
    
	ListView ManasikList;
	ArrayAdapter<String> ManasikListAdapter ;
	
	
    //AdMob
  	AdView adView;
  	RelativeLayout adlayout;
  	AdRequest request;
  	String publisherId=Settings.adMobPulisherId;
  	String testingDeviceId=Settings.testingDeviceId;

	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manasik);
        
        //place of ad
        adlayout = (RelativeLayout)findViewById(R.id.adLayout);
        //adview
       adView = new AdView(this, AdSize.BANNER, publisherId);
        
		//Get Screen 
	    Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
	    int screenWidth = display.getWidth(); 
	    Log.e("Screen Width",""+screenWidth);

	 // AdMob Size Initialisations 
	    if(screenWidth>300 && screenWidth<=320)
	    	adView = new AdView(this, AdSize.BANNER, publisherId);
	    else if(screenWidth>320 && screenWidth<=468)
	    	adView = new AdView(this, AdSize.IAB_BANNER, publisherId);
	    else if(screenWidth>468)
	    	adView = new AdView(this, AdSize.IAB_LEADERBOARD, publisherId);
	    
	    adlayout.addView(adView);
	    
	    //AdMob Request
	    request = new AdRequest();
	    
	    //only for testing Devices
	    request.addTestDevice(AdRequest.TEST_EMULATOR);
	    request.addTestDevice(testingDeviceId);
	    
	    //load Ad
	    adView.loadAd(request);
        
        
        Manasik = (Button)findViewById(R.id.manasikBtn);
        Stream = (Button)findViewById(R.id.streamBtn);
        History = (Button)findViewById(R.id.historyBtn);
        Quotes = (Button)findViewById(R.id.quotesBtn);
        
        Manasik.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),Manasik.class));
            	finish();
            }
            
        });
        
        Stream.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),LiveStream.class));
            	finish();
            }
            
        });
        
        
        History.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),History.class));
            	finish();
            }
            
        });
        
        Quotes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),QuranHadithRefs.class));
            	finish();
            }
            
        });
        
        
        
        share = (Button)findViewById(R.id.btnshare);
        back = (Button)findViewById(R.id.btnback);
       
        
        back.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				finish();
			 		
			}});
        
        
        mFacebook = new Facebook(Settings.APP_ID);
        mAsyncRunner = new AsyncFacebookRunner(mFacebook);
        
        
        
        
      
        
        
        
     // Share Button Opens the dialog for facebook connectivity and sharing posts;        
        share.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, Settings.MessageForShare);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                  
//                  JSONObject attachment = new JSONObject();
//                try {
//
//                	attachment.put("message",Settings.messageFb);
//                    attachment.put("name",Settings.AppNameFb);
//                    attachment.put("href",Settings.UrlFb);
//                    attachment.put("description",Settings.DescriptionFb);
//
//
//          } catch (JSONException e) {
//           // TODO Auto-generated catch block
//           e.printStackTrace();
//          }
//
//                Bundle params = new Bundle();
//                params.putString("attachment", attachment.toString());
//
//                mFacebook.dialog(Manasik.this, "stream.publish", params,  new WallPostDialogListener());
            }
        });

        
        
        
        ManasikList = (ListView) findViewById( R.id.ManasikListView);  
        
        String[] planets = new String[] { "1 Tawaf",
        	    "2 Arafat",
        	    "3 Muzdalifah",
        	    "4 Ramy al-Jamarat",
        	    "5 Eid al-Adha",
        	    "6 Tawaf Al-Ifaadah",
        	    "7 Tawaf al-Wida",
        	    "8 Journey to Medina",
        	    "9 Umrah"};    
ArrayList<String> planetList = new ArrayList<String>();  
  

// Create ArrayAdapter using the planet list.  
ManasikListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,planets);

ManasikList.setAdapter(ManasikListAdapter);


//On Clicking Any Item Of The List View A New Activity Will Be Opened For Respective Manasik:
ManasikList.setOnItemClickListener(new OnItemClickListener (){
		public void onItemClick(AdapterView<?> argument, View view, int id, long name) {
			
			switch( id )
		    {
		       case 0:  startActivity(new Intent(getApplicationContext(),Tawaf.class));
		                break;
		       
		       case 1:  startActivity(new Intent(getApplicationContext(),Arafat.class));
		                break;
		       
		       case 2:  startActivity(new Intent(getApplicationContext(),Muzdalifah.class));
		                break;
		       
		       case 3:  startActivity(new Intent(getApplicationContext(),RamiJamarat.class));
		                break;
		       
		       case 4:  startActivity(new Intent(getApplicationContext(),EidAlAdha.class));
		                break;
		                
		       
               case 5:  startActivity(new Intent(getApplicationContext(),TawafAlifaadah.class));
                        break;
      
               case 6:  startActivity(new Intent(getApplicationContext(),TawafAlwida.class));
                        break;
      
               case 7:  startActivity(new Intent(getApplicationContext(),JourneyMadinah.class));
                        break;
      
               case 8:  startActivity(new Intent(getApplicationContext(),Umrah.class));
                        break;
		                
		                
		    }	    	
    		
		}});


   
    }
    /***** FB Connect Work here **/
    //////////////////////////////////////////////////////////////////////
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Log.d("FB Sample App", "onActivityResult(): " + requestCode);
    mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    
    
    
    



   /**
    * WallPostRequestListener implements a request lister/callback
    *  for "wall post requests"
    */
      public class WallPostRequestListener implements
            com.facebook.android.AsyncFacebookRunner.RequestListener {
          
        /**
         * Called when the wall post request has completed
         */
        public void onComplete(final String response,Object fbo) {
          //  Log.d("Facebook-Example", "Got response: " + response);

      
        }

        public void onFacebookError(FacebookError e,Object o) {
            // Ignore Facebook errors
        }

        public void onFileNotFoundException(FileNotFoundException e,Object o) {
            // Ignore File not found errors
        }

        public void onIOException(IOException e,Object o) {
            // Ignore IO Facebook errors
        }


        public void onMalformedURLException(MalformedURLException e,Object o) {
            // Ignore Malformed URL errors
        }

   }

      
      
      
    ////////////////////////////////////////////////////////////////////////
    //// Wall post dialog completion listener
    ////////////////////////////////////////////////////////////////////////

    /**
     * WallPostDialogListener implements a dialog lister/callback
     */
    public class WallPostDialogListener implements
            com.facebook.android.Facebook.DialogListener {

        /**
         * Called when the dialog has completed successfully
         */
        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null) {
               // Log.d("FB Sample App", "Dialog Success! post_id=" + postId);
            	mAsyncRunner.request(postId, new WallPostRequestListener());
                
            } else {
               // Log.d("FB Sample App", "No wall post made");
            }
        }


        public void onCancel() {
            // No special processing if dialog has been canceled
        }


        public void onError(DialogError e) {
            // No special processing if dialog has been canceled
        }


        public void onFacebookError(FacebookError e) {
            // No special processing if dialog has been canceled
        }
    }

    /////////////////////////////////////////////////////////
    // Login / Logout Listeners
    /////////////////////////////////////////////////////////
    
    /**
     * Listener for login dialog completion status
     */
    public final class LoginDialogListener implements
             com.facebook.android.Facebook.DialogListener {
    
         /**
          * Called when the dialog has completed successfully
          */
         public void onComplete(Bundle values) {
             // Process onComplete
             Log.d("FB Sample App", "LoginDialogListener.onComplete()");
             // Dispatch on its own thread
            
    
    //         mHandler.post(new Runnable() {
    //             public void run() {
                     //mText.setText("Facebook login successful. Press Menu...");
                  
               //mSpinner.show();
                 // mAsyncRunner.request("me",new GetIdListener());
                  
    //             }
    //         });
         }
    
         /**
          *
          */
         public void onFacebookError(FacebookError error) {
             // Process error
         //    Log.d("FB Sample App", "LoginDialogListener.onFacebookError()");
         }
    
         /**
          *
          */
         public void onError(DialogError error) {
             // Process error message
       //     Log.d("FB Sample App", "LoginDialogListener.onError()");
         }
    
         /**
          *
          */
         public void onCancel() {
             // Process cancel message
      //      Log.d("FB Sample App", "LoginDialogListener.onCancel()");
    }
    }
    
    /**
     * Listener for logout status message
     */
    public class LogoutRequestListener implements 
         com.facebook.android.AsyncFacebookRunner.RequestListener {
    
         /** Called when the request completes w/o error */
         public void onComplete(String response,Object fbo) {
    
             // Only the original owner thread can touch its views
             runOnUiThread(new Runnable() {
                 public void run() {
             
                     //mFacebook.setText("Thanks for using FB Sample App. Bye bye...");
                     //friends.clear();
                    // friendsArrayAdapter.notifyDataSetChanged();
                	

                 }
             });
    
             Handler handler = null;
			// Dispatch on its own thread
             handler.post(new Runnable() {
                 public void run() {
                  
                 }
             });
         }
    
         public void onFacebookError(FacebookError e,Object o) {
             // Process Facebook error message
          
         }
    
         public void onFileNotFoundException(FileNotFoundException e,Object o) {
             // Process Exception
         }
    
         public void onIOException(IOException e,Object o) {
             // Process Exception
         }
    
         public void onMalformedURLException(MalformedURLException e,Object o) {
             // Process Exception
         }
    
    }
 
    
    
    
    
    
    
    
    
}