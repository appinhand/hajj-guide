package com.appinhand.hajjguideapp;
import java.util.List;

import com.appinhand.hajjguideapp.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class HadithListAdaptor extends ArrayAdapter<Hadith> {
	
	private final List<Hadith> list;
	private final Activity context;
	ViewHolder holder;
	

	public HadithListAdaptor(Activity context, List<Hadith> list) {
		super(context, R.layout.row, list);
		this.context = context;
		this.list = list;
	}

	static class ViewHolder {
		protected TextView narrator_view;
		protected TextView detail_view;
		
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View view = null;

		if (convertView == null) {
			LayoutInflater inflator = context.getLayoutInflater();
			view = inflator.inflate(R.layout.row, null);
			final ViewHolder viewHolder = new ViewHolder();
			
			viewHolder.narrator_view = (TextView) view.findViewById(R.id.nar_text);
			viewHolder.detail_view = (TextView) view.findViewById(R.id.det_text);
		
			view.setTag(viewHolder);	
		} else {
			view = convertView;
	}
		


		holder = (ViewHolder) view.getTag();
		
		holder.narrator_view.setText("Hazrat "+list.get(position).getNarrator());

		holder.detail_view.setText("Volume:"+list.get(position).getVolume()+", Book:"+list.get(position).getBook() + ", Number:" + list.get(position).getNumber());
		     
		return view;	
}
	


	
	
}




