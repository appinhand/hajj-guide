package com.appinhand.hajjguideapp;

public class Verse {
	
	public String rowid;
	public String Ayat;
	public String VERSE;
	public String surah;
	

	
	Verse(String rowid,String Ayat,String VERSE,String surah) 
	{
    this.rowid=rowid;
    this.Ayat=Ayat;
	this.VERSE=VERSE;
	this.surah=surah;
	
	}
	
	public String getRowId() {
		return rowid;
	}
	
	public void setRowId(String rowid) {
		this.rowid = rowid;
	}
	

	public String getAyat() {
		return Ayat;
	}
	
	public void setAyat(String Ayat) {
		this.Ayat = Ayat;
	}
	
	
	public String getVERSE() {
		return VERSE;
	}
	
	public void setVERSE(String VERSE) {
		this.VERSE = VERSE;
	}
	
	
	public String getSurah() {
		return surah;
	}
	
	public void setSurah(String surah) {
		this.surah = surah;
	}
	
	
	
	
	
	

}
