package com.appinhand.hajjguideapp;
import java.util.List;

import com.appinhand.hajjguideapp.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class VerseAdaptor extends ArrayAdapter<Verse> {
	
	private final List<Verse> list;
	private final Activity context;
	ViewHolder holder;
	

	public VerseAdaptor(Activity context, List<Verse> list) {
		super(context, R.layout.verserow, list);
		this.context = context;
		this.list = list;
	}

	static class ViewHolder {
		protected TextView verse_view;
		protected TextView desc_view;
		
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View view = null;

		if (convertView == null) {
			LayoutInflater inflator = context.getLayoutInflater();
			view = inflator.inflate(R.layout.verserow, null);
			final ViewHolder viewHolder = new ViewHolder();
			
			viewHolder.verse_view = (TextView) view.findViewById(R.id.verse_text);
			viewHolder.desc_view = (TextView) view.findViewById(R.id.descp_text);
		
			view.setTag(viewHolder);	
		} else {
			view = convertView;
	}
		


		holder = (ViewHolder) view.getTag();
		
		holder.verse_view.setText(list.get(position).getVERSE());

		holder.desc_view.setText("Ayat # : "+list.get(position).getAyat()+",Surah # :"+list.get(position).getSurah());
		     
		return view;	
}
	


	
	
}




