package com.appinhand.hajjguideapp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.appinhand.hajjguideapp.R;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;


public class HajjFirst extends Activity {
    
	Button share,home,info,Exit,back,Next,Prev;
	TextView AboutTextView,Heading;
	
	ImageView Stepview;
	ScrollView sv1;
	public static Facebook mFacebook;
    public static AsyncFacebookRunner mAsyncRunner;
    public static final String APP_ID = "221442861316255";
	
    int z;
    String[] HajjStepsDetail = new String[100];
    String[] HajjStepsHeading = {"First Day","Second Day","Third Day","Fourth Day","Fifth Day"};
	int i=0;
	int[] hajjint =  {R.string.first_day,R.string.sec_day,R.string.third_day,R.string.fourth_day,R.string.fifth_day};
    Button Manasik,Stream,History,Quotes;
	
    
    int[] StepImages = {R.drawable.step1,R.drawable.step2,R.drawable.step3,R.drawable.step4,R.drawable.step5};
    
    
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hajjfirstday);
        
     


        Manasik = (Button)findViewById(R.id.manasikBtn);
        Stream = (Button)findViewById(R.id.streamBtn);
        History = (Button)findViewById(R.id.streamBtn);
        Quotes = (Button)findViewById(R.id.quotesBtn);
        back = (Button)findViewById(R.id.btnback);
        Next = (Button)findViewById(R.id.nextBtn);
        Prev = (Button)findViewById(R.id.prevBtn);
        Heading = (TextView)findViewById(R.id.textView1);
        AboutTextView = (TextView) findViewById(R.id.textView2);
        Stepview = (ImageView)findViewById(R.id.stepsimageView);
        sv1 = (ScrollView)findViewById(R.id.scrollView1);
        
        
        Stepview.setImageResource(StepImages[0]);
        
        Heading.setText(HajjStepsHeading[0]);
        AboutTextView.setText(Html.fromHtml(getString(R.string.first_day)));
        
      
          for(int x=0; x<5; x++)
          {
	        
        	  HajjStepsDetail[x]=Html.fromHtml(getString(hajjint[x])).toString();
        	 

          }
        
   
        
  
        
        Next.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
		
            	
              if(i<4)
              {     i++;
            		AboutTextView.setText(HajjStepsDetail[i]);
                	Heading.setText(HajjStepsHeading[i]);
                	Stepview.setImageResource(StepImages[i]);
                	sv1.scrollTo(0,0);
                	
                	
              }
              
              
     
     
			}});
        
       
        
        Prev.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
		
            	
              if(i>0)
              {     i--;
            		AboutTextView.setText(HajjStepsDetail[i]);
                	Heading.setText(HajjStepsHeading[i]);
                	Stepview.setImageResource(StepImages[i]);
                	sv1.scrollTo(0,0);
                	
              }
              
              
     
     
			}});
        
        
        
        back.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(getApplicationContext(),Options.class));
				finish();
			 		
			}});
        
        Manasik.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),Manasik.class));
            	finish();
            }
            
        });
        
        Stream.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),LiveStream.class));
            	finish();
            }
            
        });
        
        
        History.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),History.class));
            	finish();
            }
            
        });
        
        Quotes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),QuranHadithRefs.class));
            	finish();
            }
            
        });
        
        
        
        
       share = (Button)findViewById(R.id.btnshare);
       
       
       
       
        
   
        
        mFacebook = new Facebook(APP_ID);
        mAsyncRunner = new AsyncFacebookRunner(mFacebook);
        
        
        

        
        
     // Share Button Opens the dialog for facebook connectivity and sharing posts;        
        share.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, Settings.MessageForShare);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
//                  JSONObject attachment = new JSONObject();
//                try {
//
//                	attachment.put("message","Hajj App By AppXone");
//                    attachment.put("name","Hajj Guide 1.00");
//                    attachment.put("href","http://www.appxone.com");
//                    attachment.put("description","AppXone");
//
//
//          } catch (JSONException e) {
//           // TODO Auto-generated catch block
//           e.printStackTrace();
//          }
//
//                Bundle params = new Bundle();
//                params.putString("attachment", attachment.toString());
//
//                mFacebook.dialog(HajjFirst.this, "stream.publish", params,  new WallPostDialogListener());
            }
        });

        

        
        
        
    }
    
    
    /***** FB Connect Work here **/
    //////////////////////////////////////////////////////////////////////
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Log.d("FB Sample App", "onActivityResult(): " + requestCode);
    mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    
    
    
    



   /**
    * WallPostRequestListener implements a request lister/callback
    *  for "wall post requests"
    */
      public class WallPostRequestListener implements
            com.facebook.android.AsyncFacebookRunner.RequestListener {
          
        /**
         * Called when the wall post request has completed
         */
        public void onComplete(final String response,Object fbo) {
          //  Log.d("Facebook-Example", "Got response: " + response);

      
        }

        public void onFacebookError(FacebookError e,Object o) {
            // Ignore Facebook errors
        }

        public void onFileNotFoundException(FileNotFoundException e,Object o) {
            // Ignore File not found errors
        }

        public void onIOException(IOException e,Object o) {
            // Ignore IO Facebook errors
        }


        public void onMalformedURLException(MalformedURLException e,Object o) {
            // Ignore Malformed URL errors
        }

   }

      
      
      
    ////////////////////////////////////////////////////////////////////////
    //// Wall post dialog completion listener
    ////////////////////////////////////////////////////////////////////////

    /**
     * WallPostDialogListener implements a dialog lister/callback
     */
    public class WallPostDialogListener implements
            com.facebook.android.Facebook.DialogListener {

        /**
         * Called when the dialog has completed successfully
         */
        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null) {
               // Log.d("FB Sample App", "Dialog Success! post_id=" + postId);
            	mAsyncRunner.request(postId, new WallPostRequestListener());
                
            } else {
               // Log.d("FB Sample App", "No wall post made");
            }
        }


        public void onCancel() {
            // No special processing if dialog has been canceled
        }


        public void onError(DialogError e) {
            // No special processing if dialog has been canceled
        }


        public void onFacebookError(FacebookError e) {
            // No special processing if dialog has been canceled
        }
    }

    /////////////////////////////////////////////////////////
    // Login / Logout Listeners
    /////////////////////////////////////////////////////////
    
    /**
     * Listener for login dialog completion status
     */
    public final class LoginDialogListener implements
             com.facebook.android.Facebook.DialogListener {
    
         /**
          * Called when the dialog has completed successfully
          */
         public void onComplete(Bundle values) {
             // Process onComplete
             Log.d("FB Sample App", "LoginDialogListener.onComplete()");
             // Dispatch on its own thread
            
    
    //         mHandler.post(new Runnable() {
    //             public void run() {
                     //mText.setText("Facebook login successful. Press Menu...");
                  
               //mSpinner.show();
                 // mAsyncRunner.request("me",new GetIdListener());
                  
    //             }
    //         });
         }
    
         /**
          *
          */
         public void onFacebookError(FacebookError error) {
             // Process error
         //    Log.d("FB Sample App", "LoginDialogListener.onFacebookError()");
         }
    
         /**
          *
          */
         public void onError(DialogError error) {
             // Process error message
       //     Log.d("FB Sample App", "LoginDialogListener.onError()");
         }
    
         /**
          *
          */
         public void onCancel() {
             // Process cancel message
      //      Log.d("FB Sample App", "LoginDialogListener.onCancel()");
    }
    }
    
    /**
     * Listener for logout status message
     */
    public class LogoutRequestListener implements 
         com.facebook.android.AsyncFacebookRunner.RequestListener {
    
         /** Called when the request completes w/o error */
         public void onComplete(String response,Object fbo) {
    
             // Only the original owner thread can touch its views
             runOnUiThread(new Runnable() {
                 public void run() {
             
                     //mFacebook.setText("Thanks for using FB Sample App. Bye bye...");
                     //friends.clear();
                    // friendsArrayAdapter.notifyDataSetChanged();
                	

                 }
             });
    
             Handler handler = null;
			// Dispatch on its own thread
             handler.post(new Runnable() {
                 public void run() {
                  
                 }
             });
         }
    
         public void onFacebookError(FacebookError e,Object o) {
             // Process Facebook error message
          
         }
    
         public void onFileNotFoundException(FileNotFoundException e,Object o) {
             // Process Exception
         }
    
         public void onIOException(IOException e,Object o) {
             // Process Exception
         }
    
         public void onMalformedURLException(MalformedURLException e,Object o) {
             // Process Exception
         }
    
    }
    
    
    
    
    
    
    
}