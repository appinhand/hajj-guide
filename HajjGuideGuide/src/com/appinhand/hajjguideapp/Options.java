package com.appinhand.hajjguideapp;
import com.appinhand.hajjguideapp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Options extends Activity {
	
	Button info,history,manasik,qhrefs,stream,Exit,more,Settings,HajjSteps;
//	ImageView alphaBackground;
	
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.options);
        
        
        
        Intent svc=new Intent(Options.this,BackgroundSoundService.class);
        this.startService(svc);
        
        
        
        info = (Button)findViewById(R.id.infoButton);
        history = (Button)findViewById(R.id.btnHistory);
        manasik = (Button)findViewById(R.id.btnManasik);
        qhrefs = (Button)findViewById(R.id.qhrefs);
        stream = (Button)findViewById(R.id.btnStream);
        more = (Button)findViewById(R.id.btnMore);
        HajjSteps = (Button)findViewById(R.id.hajjSteps);

        
     // Settings = (Button)findViewById(R.id.settingsBtn);
        
//      alphaBackground = (ImageView) findViewById(R.id.alphaBackground);
//      alphaBackground.getBackground().setAlpha(100);
      
//      Animation myFadeInAnimation = AnimationUtils.loadAnimation(Options.this, R.anim.tween);
//      alphaBackground.startAnimation(myFadeInAnimation);
        
      
      
      HajjSteps.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(),HajjFirst.class));
			 		
			}});
        
        info.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(getApplicationContext(),About.class));
            	
				
			}});
        
        history.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(getApplicationContext(),History.class));
            	
				
			}});
        
        manasik.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(getApplicationContext(),Manasik.class));
            	
			}});
        
        qhrefs.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(getApplicationContext(),QuranHadithRefs.class));
            
			}});
        
        stream.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(getApplicationContext(),LiveStream.class));
            	
				
			}});
        
 
        more.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(getApplicationContext(),MoreApps.class));
            	
				
			}});
        
       
//        Settings.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				
//				startActivity(new Intent(getApplicationContext(),AppSettings.class));
//				
//			}});
        
        
        
        
        
        
        
        
    }
    

    protected void onPause() {
    // TODO Auto-generated method stub
    super.onDestroy();

    Intent svc=new Intent(Options.this,BackgroundSoundService.class);
    this.stopService(svc);
    
    }
    
    
    
    
    
    
}