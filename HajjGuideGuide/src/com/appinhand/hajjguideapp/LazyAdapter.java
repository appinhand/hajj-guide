package com.appinhand.hajjguideapp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import com.appinhand.hajjguideapp.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LazyAdapter extends BaseAdapter {
    public static String LinkUrl;
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public LazyAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_row, null);

        TextView titletxt = (TextView)vi.findViewById(R.id.title); // title
        TextView subtitletxt = (TextView)vi.findViewById(R.id.subtitle); // artist name
        TextView infotxt = (TextView)vi.findViewById(R.id.info); // duration
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.list_image); // thumb image
        
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);
        
        // Setting all values in listview
        titletxt.setText(song.get(MoreApps.KEY_TITLE));
        subtitletxt.setText(song.get(MoreApps.KEY_SUBTITLE));
        infotxt.setText(song.get(MoreApps.KEY_INFO));
        //imageLoader.DisplayImage(song.get(MoreApps.KEY_THUMB_URL), thumb_image);
        
        LinkUrl = song.get(MoreApps.KEY_LINK_URL).toString();
        
        
        String imageUrl = song.get(MoreApps.KEY_THUMB_URL).toString();
       
       URL url = null;
		try {
			url = new URL(imageUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       Bitmap bmp = null;
		try {
			bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       thumb_image.setImageBitmap(bmp);
       
       
        return vi;
    }
}