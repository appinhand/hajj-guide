package com.appinhand.hajjguideapp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.appinhand.hajjguideapp.R;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
public class MoreApps extends Activity {
	
	Button share,home,info,Exit,back;
	// XML node keys
	public static final String KEY_APP = "app"; // parent node
	public static final String KEY_ID = "id";
	public static final String KEY_TITLE = "title";
	public static final String KEY_SUBTITLE = "subtitle";
	public static final String KEY_INFO = "info";
	public static final String KEY_THUMB_URL = "thumb_url";
	public static final String KEY_LINK_URL = "link_url";
	ListView list;
    LazyAdapter adapter;
    ArrayList<HashMap<String, String>> hadithsList;
	
    Button Manasik,History,Stream,Quotes;
	
	//Initializing the dbmanager object
	DataBaseManager dbmanager = new DataBaseManager(this);
	
	
	
	public static Facebook mFacebook;
    public static AsyncFacebookRunner mAsyncRunner;
    public static final String APP_ID = Settings.APP_ID;
	


	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_apps);
        
     
        
        // listview xml work
        hadithsList = new ArrayList<HashMap<String, String>>();

		XMLParser parser = new XMLParser();
		String xml = parser.getXmlFromUrl(Settings.MoreAppsURL); // getting XML from URL
		Document doc = parser.getDomElement(xml); // getting DOM element
		
		NodeList nl = doc.getElementsByTagName(KEY_APP);
		// looping through all song nodes <song>
		for (int i = 0; i < nl.getLength(); i++) {
			// creating new HashMap
			HashMap<String, String> map = new HashMap<String, String>();
			Element e = (Element) nl.item(i);
			// adding each child node to HashMap key => value
			map.put(KEY_ID, parser.getValue(e, KEY_ID));
			map.put(KEY_TITLE, parser.getValue(e, KEY_TITLE));
			map.put(KEY_SUBTITLE, parser.getValue(e, KEY_SUBTITLE));
			map.put(KEY_INFO, parser.getValue(e, KEY_INFO));
			map.put(KEY_THUMB_URL, parser.getValue(e, KEY_THUMB_URL));
			map.put(KEY_LINK_URL, parser.getValue(e, KEY_LINK_URL));
			// adding HashList to ArrayList
			hadithsList.add(map);
		}
		
        
		list=(ListView)findViewById(R.id.list);
		
		// Getting adapter by passing xml data ArrayList
        adapter=new LazyAdapter(this, hadithsList);        
        list.setAdapter(adapter);
        String linkUrl;
       

        // Click event for single list row
        	
        Manasik = (Button)findViewById(R.id.manasikBtn);
        Stream = (Button)findViewById(R.id.streamBtn);
        History = (Button)findViewById(R.id.historyBtn);
        Quotes = (Button)findViewById(R.id.quotesBtn);
        
        Manasik.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),Manasik.class));
            	finish();
            }
            
        });
        
        Stream.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),LiveStream.class));
            	finish();
            }
            
        });
        
        
        History.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),History.class));
            	finish();
            }
            
        });
        
        Quotes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),QuranHadithRefs.class));
            	finish();
            }
            
        });
        
        
        share = (Button)findViewById(R.id.btnshare);
        back = (Button)findViewById(R.id.btnback);
       
back = (Button)findViewById(R.id.btnback);
        
        
        back.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				finish();
			 		
			}});
  
        
        list.setOnItemClickListener(new OnItemClickListener (){
  			public void onItemClick(AdapterView<?> argument, View view, int id, long name) {
  			
  				
  				
  			//getting signature
  				boolean isMarketSig = false;
  			    int currentSig = 1; // I just set this to 1 to avoid any exceptions later on.
  			    try {
  			        Signature[] sigs = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES).signatures;
  			        for (Signature sig : sigs)
  			        {
  			            currentSig = sig.hashCode();


  			    Log.e("MyApp", "Signature hashcode : " + sig.hashCode());
  			// This Log is for first time testing so you can find out what the int value of your signature is.
  			        }
	            } catch (Exception e){
	                e.printStackTrace();
  			}
  			   
		    //if signature matches with Application signature, it means app was installed from Google Play
		    //because Amazon Appstore has its own signatures

  			if (currentSig==924749328)
  			{
	        	Intent goToMarket = new Intent(Intent.ACTION_VIEW,Uri.parse(hadithsList.get(id).get(KEY_LINK_URL)));
	        	goToMarket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        	startActivity(goToMarket);  
	        } 
	        else
	        {
		    	Intent goToAppStore = new Intent(Intent.ACTION_VIEW,Uri.parse(hadithsList.get(id).get(KEY_LINK_URL)));
		    	goToAppStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    	    startActivity(goToAppStore);  
	        }

  			
  			
//  			String myUrl = hadithsList.get(id).get(KEY_LINK_URL);
//			String applicationTitle=hadithsList.get(id).get(KEY_TITLE);
//			Intent detail_intent = new Intent(MoreApps.this,MoreAppsDetail.class);
//			detail_intent.putExtra("WebUrl",myUrl);
//			detail_intent.putExtra("applicationTitle",applicationTitle);
//			startActivity(detail_intent);
  			        
  				
  					    			    	
  			
  			
  			}});
		
        
        
        
        mFacebook = new Facebook(Settings.APP_ID);
        mAsyncRunner = new AsyncFacebookRunner(mFacebook);
	    
        share.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, Settings.MessageForShare);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
//                JSONObject attachment = new JSONObject();
//                try {
//                 attachment.put("message", Settings.messageFb);
//                 attachment.put("name", Settings.AppNameFb);
//                 attachment.put("href", Settings.WebSiteURL);
//                 attachment.put("description",Settings.DescriptionFb);
//
//          } catch (JSONException e) {
//           // TODO Auto-generated catch block
//           e.printStackTrace();
//          }
//
//                Bundle params = new Bundle();
//                params.putString("attachment", attachment.toString());
//
//                mFacebook.dialog(MoreApps.this, "stream.publish", params,  new WallPostDialogListener());
            }
        });

       
        
            
        
        
        
    }






    /***** FB Connect Work here **/
    //////////////////////////////////////////////////////////////////////
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Log.d("FB Sample App", "onActivityResult(): " + requestCode);
    mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    
    
    
    



   /**
    * WallPostRequestListener implements a request lister/callback
    *  for "wall post requests"
    */
      public class WallPostRequestListener implements
            com.facebook.android.AsyncFacebookRunner.RequestListener {

        /**
         * Called when the wall post request has completed
         */
        public void onComplete(final String response,Object fbo) {
          //  Log.d("Facebook-Example", "Got response: " + response);

      
        }

        public void onFacebookError(FacebookError e,Object o) {
            // Ignore Facebook errors
        }

        public void onFileNotFoundException(FileNotFoundException e,Object o) {
            // Ignore File not found errors
        }

        public void onIOException(IOException e,Object o) {
            // Ignore IO Facebook errors
        }


        public void onMalformedURLException(MalformedURLException e,Object o) {
            // Ignore Malformed URL errors
        }

   }

      
      
      
    ////////////////////////////////////////////////////////////////////////
    //// Wall post dialog completion listener
    ////////////////////////////////////////////////////////////////////////

    /**
     * WallPostDialogListener implements a dialog lister/callback
     */
    public class WallPostDialogListener implements
            com.facebook.android.Facebook.DialogListener {

        /**
         * Called when the dialog has completed successfully
         */
        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null) {
               // Log.d("FB Sample App", "Dialog Success! post_id=" + postId);
            	mAsyncRunner.request(postId, new WallPostRequestListener());
            } else {
               // Log.d("FB Sample App", "No wall post made");
            }
        }


        public void onCancel() {
            // No special processing if dialog has been canceled
        }


        public void onError(DialogError e) {
            // No special processing if dialog has been canceled
        }


        public void onFacebookError(FacebookError e) {
            // No special processing if dialog has been canceled
        }
    }

    /////////////////////////////////////////////////////////
    // Login / Logout Listeners
    /////////////////////////////////////////////////////////
    
    /**
     * Listener for login dialog completion status
     */
    public final class LoginDialogListener implements
             com.facebook.android.Facebook.DialogListener {
    
         /**
          * Called when the dialog has completed successfully
          */
         public void onComplete(Bundle values) {
             // Process onComplete
             Log.d("FB Sample App", "LoginDialogListener.onComplete()");
             // Dispatch on its own thread
            
    
    //         mHandler.post(new Runnable() {
    //             public void run() {
                     //mText.setText("Facebook login successful. Press Menu...");
                  
               //mSpinner.show();
                 // mAsyncRunner.request("me",new GetIdListener());
                  
    //             }
    //         });
         }
    
         /**
          *
          */
         public void onFacebookError(FacebookError error) {
             // Process error
         //    Log.d("FB Sample App", "LoginDialogListener.onFacebookError()");
         }
    
         /**
          *
          */
         public void onError(DialogError error) {
             // Process error message
       //     Log.d("FB Sample App", "LoginDialogListener.onError()");
         }
    
         /**
          *
          */
         public void onCancel() {
             // Process cancel message
      //      Log.d("FB Sample App", "LoginDialogListener.onCancel()");
    }
    }
    
    /**
     * Listener for logout status message
     */
    public class LogoutRequestListener implements 
         com.facebook.android.AsyncFacebookRunner.RequestListener {
    
         /** Called when the request completes w/o error */
         public void onComplete(String response,Object fbo) {
    
             // Only the original owner thread can touch its views
             runOnUiThread(new Runnable() {
                 public void run() {
                    // mText.setText("Thanks for using FB Sample App. Bye bye...");
                     //friends.clear();
                    // friendsArrayAdapter.notifyDataSetChanged();
                 }
             });
    
             Handler handler = null;
			// Dispatch on its own thread
             handler.post(new Runnable() {
                 public void run() {
                  
                 }
             });
         }
    
         public void onFacebookError(FacebookError e,Object o) {
             // Process Facebook error message
          
         }
    
         public void onFileNotFoundException(FileNotFoundException e,Object o) {
             // Process Exception
         }
    
         public void onIOException(IOException e,Object o) {
             // Process Exception
         }
    
         public void onMalformedURLException(MalformedURLException e,Object o) {
             // Process Exception
         }
    
    }
    
    
}       

    
    
    
    

