package com.appinhand.hajjguideapp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.appinhand.hajjguideapp.R;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;

public class TawafAlifaadah extends Activity {
    
	Button share,home,info,back;
	public static Facebook mFacebook;
    public static AsyncFacebookRunner mAsyncRunner;
    
	
    Button Manasik,History,Stream,Quotes;
	
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tawafalafaadah);
        
        
        Manasik = (Button)findViewById(R.id.manasikBtn);
        Stream = (Button)findViewById(R.id.streamBtn);
        History = (Button)findViewById(R.id.historyBtn);
        Quotes = (Button)findViewById(R.id.quotesBtn);
        
        Manasik.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),Manasik.class));
            	finish();
            }
            
        });
        
        Stream.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),LiveStream.class));
            	finish();
            }
            
        });
        
        
        History.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),History.class));
            	finish();
            }
            
        });
        
        Quotes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {    
        
            	startActivity(new Intent(getApplicationContext(),QuranHadithRefs.class));
            	finish();
            }
            
        });
        
        
        share = (Button)findViewById(R.id.btnshare);
        back = (Button)findViewById(R.id.btnback);
        
        
        back.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				finish();
			 		
			}});
        
        
        mFacebook = new Facebook(Settings.APP_ID);
        mAsyncRunner = new AsyncFacebookRunner(mFacebook);
        
        
    
        
        
        
     // Share Button Opens the dialog for facebook connectivity and sharing posts;        
        share.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, Settings.MessageForShare);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
//
//                  JSONObject attachment = new JSONObject();
//                try {
//
//                	attachment.put("message",Settings.messageFb);
//                    attachment.put("name",Settings.AppNameFb);
//                    attachment.put("href",Settings.UrlFb);
//                    attachment.put("description",Settings.DescriptionFb);
//
//
//          } catch (JSONException e) {
//           // TODO Auto-generated catch block
//           e.printStackTrace();
//          }
//
//                Bundle params = new Bundle();
//                params.putString("attachment", attachment.toString());
//
//                mFacebook.dialog(TawafAlifaadah.this, "stream.publish", params,  new WallPostDialogListener());
            }
        });

        

        
        
        
    }
    
    
    /***** FB Connect Work here **/
    //////////////////////////////////////////////////////////////////////
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Log.d("FB Sample App", "onActivityResult(): " + requestCode);
    mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    
    
    
    



   /**
    * WallPostRequestListener implements a request lister/callback
    *  for "wall post requests"
    */
      public class WallPostRequestListener implements
            com.facebook.android.AsyncFacebookRunner.RequestListener {
          
        /**
         * Called when the wall post request has completed
         */
        public void onComplete(final String response,Object fbo) {
          //  Log.d("Facebook-Example", "Got response: " + response);

      
        }

        public void onFacebookError(FacebookError e,Object o) {
            // Ignore Facebook errors
        }

        public void onFileNotFoundException(FileNotFoundException e,Object o) {
            // Ignore File not found errors
        }

        public void onIOException(IOException e,Object o) {
            // Ignore IO Facebook errors
        }


        public void onMalformedURLException(MalformedURLException e,Object o) {
            // Ignore Malformed URL errors
        }

   }

      
      
      
    ////////////////////////////////////////////////////////////////////////
    //// Wall post dialog completion listener
    ////////////////////////////////////////////////////////////////////////

    /**
     * WallPostDialogListener implements a dialog lister/callback
     */
    public class WallPostDialogListener implements
            com.facebook.android.Facebook.DialogListener {

        /**
         * Called when the dialog has completed successfully
         */
        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null) {
               // Log.d("FB Sample App", "Dialog Success! post_id=" + postId);
            	mAsyncRunner.request(postId, new WallPostRequestListener());
                
            } else {
               // Log.d("FB Sample App", "No wall post made");
            }
        }


        public void onCancel() {
            // No special processing if dialog has been canceled
        }


        public void onError(DialogError e) {
            // No special processing if dialog has been canceled
        }


        public void onFacebookError(FacebookError e) {
            // No special processing if dialog has been canceled
        }
    }

    /////////////////////////////////////////////////////////
    // Login / Logout Listeners
    /////////////////////////////////////////////////////////
    
    /**
     * Listener for login dialog completion status
     */
    public final class LoginDialogListener implements
             com.facebook.android.Facebook.DialogListener {
    
         /**
          * Called when the dialog has completed successfully
          */
         public void onComplete(Bundle values) {
             // Process onComplete
             Log.d("FB Sample App", "LoginDialogListener.onComplete()");
             // Dispatch on its own thread
            
    
    //         mHandler.post(new Runnable() {
    //             public void run() {
                     //mText.setText("Facebook login successful. Press Menu...");
                  
               //mSpinner.show();
                 // mAsyncRunner.request("me",new GetIdListener());
                  
    //             }
    //         });
         }
    
         /**
          *
          */
         public void onFacebookError(FacebookError error) {
             // Process error
         //    Log.d("FB Sample App", "LoginDialogListener.onFacebookError()");
         }
    
         /**
          *
          */
         public void onError(DialogError error) {
             // Process error message
       //     Log.d("FB Sample App", "LoginDialogListener.onError()");
         }
    
         /**
          *
          */
         public void onCancel() {
             // Process cancel message
      //      Log.d("FB Sample App", "LoginDialogListener.onCancel()");
    }
    }
    
    /**
     * Listener for logout status message
     */
    public class LogoutRequestListener implements 
         com.facebook.android.AsyncFacebookRunner.RequestListener {
    
         /** Called when the request completes w/o error */
         public void onComplete(String response,Object fbo) {
    
             // Only the original owner thread can touch its views
             runOnUiThread(new Runnable() {
                 public void run() {
             
                     //mFacebook.setText("Thanks for using FB Sample App. Bye bye...");
                     //friends.clear();
                    // friendsArrayAdapter.notifyDataSetChanged();
                	

                 }
             });
    
             Handler handler = null;
			// Dispatch on its own thread
             handler.post(new Runnable() {
                 public void run() {
                  
                 }
             });
         }
    
         public void onFacebookError(FacebookError e,Object o) {
             // Process Facebook error message
          
         }
    
         public void onFileNotFoundException(FileNotFoundException e,Object o) {
             // Process Exception
         }
    
         public void onIOException(IOException e,Object o) {
             // Process Exception
         }
    
         public void onMalformedURLException(MalformedURLException e,Object o) {
             // Process Exception
         }
    
    }
    
    
    
    
    
    
    
}