package com.appinhand.hajjguideapp;

public class Hadith {
	
	public String id;
	public String narrator;
	public String topic;
	public String volume;
	public String book;
	public String number;
	
	
	Hadith(String id,String narrator,String topic,String volume,String book,String number) 
	{
    this.id=id;
	this.narrator=narrator;
	this.topic=topic;
	this.volume=volume;
	this.book=book;
	this.number=number;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNarrator() {
		return narrator;
	}
	
	public void setNarrator(String narrator) {
		this.narrator = narrator;
	}
	
	public String getTopic() {
		return topic;
	}
	
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	public String getVolume() {
		return volume;
	}
	
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	public String getBook() {
		return book;
	}
	
	public void setBook(String book) {
		this.book = book;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	
	

}
