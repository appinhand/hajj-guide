package com.appinhand.hajjguideapp;

// Hajj Guide 1.0 FB Integration App ID = 221442861316255

import java.io.IOException;

import com.appinhand.hajjguideapp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


public class Splash extends Activity {
	
	
	DataBaseManager dbmanager = new DataBaseManager(this);
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        // For creating the database : 
        try{
            
        	dbmanager.createDataBase();
        }
        catch(IOException ex){
        
        	throw new Error("Unable to Create Database");
        	
        }
        
        
        
        
        new Handler().postDelayed(new Runnable() {
            public void run() {
           startActivity(new Intent(Splash.this, Options.class));
           
 
           finish();
            
            }
           }, 1000);        
        
        
    }
}