package com.appinhand.hajjguideapp;


public class Settings {


    // DB path and name used across the app
    public static String DB_PATH = "/data/data/com.appinhand.hajjguideapp/databases/";
    public static String DB_NAME = "hajjdb";


    //Facebook Related Settings Used Over The App's Activities
    public static final String APP_ID = "351170688300090";
    public static String messageFb = "This one is great Android Application by Appinhand";
    public static String AppNameFb = "Hajj Guide";
    public static String DescriptionFb = "Appinhand";
    public static String UrlFb = "http://www.Appinhand.net";

    public static String MessageForShare = "https://play.google.com/store/apps/details?id=com.appinhand.hajjguideapp";

    //Company Website URL
    static final String WebSiteURL = "http://www.Appinhand.net";


    //XML URL Link used in one of the Activities For Parsing data
    public static int id = 0; //
    static final String MoreAppsURL = "http://www.Appinhand.com/AppsData/Hadith/moreapps.php?id=" + id;

    //Admob Settings
    static final String adMobPulisherId = "a1505d450e32303"; //only for FREE App of Eids
    static final String testingDeviceId = "359918043312594"; //htc explorer


}
